# Exercice de programme de gestion de compte bancaire 

## Objectifs

Le programme permettra de simuler la gestion d'une application bancaire, en permettant à l'utilisateur de réaliser des opérations
sur son compte grâce à des méthodes implémentées dans le programme. Le programme devra à terme utiliser une base de données utilisateur
et disposer d'une interface graphique.

## Méthodes

Les différentes opérations pouvant être réalisées par l'utilisateur sont permises par les méthodes suivantes : afficher_solde,  retrait, 
versement, appliquer_agios, appliquer_interets. Elles permettent respectivement à l'utilisateur de consulter l'état de leur solde,
de retirer ou de verser de l'argent sur leur compte courant, de gérer les découverts avec les agios et les intérêts sur le compte épargne.

## Explication du code

Le programme de gestion de compte bancaire est composé de 3 classes : une classe mère Compte, prenant en attributs le numéro de compte, le nom
et le montant du solde de l'utilisateur:

`class Compte:
    def __init__(self, numero_compte, nom_proprietaire, solde):
        self.__numero_compte = numero_compte
        self.__nom_proprietaire = nom_proprietaire
        self.__solde = solde`

Une classe fille CompteCourant, prenant en attributs l'autorisation de découvert et le taux de pourcentage d'agios :

`class CompteCourant(Compte):
    def __init__(self, autorisation_decouvert, pourcentage_agios):
        self.autorisation_decouvert = autorisation_decouvert
        self.pourcentage_agios = pourcentage_agios`

Ainsi qu'une classe fille CompteEpargne, prenant en attributs le taux d'intérêts :

`class CompteEpargne(Compte):
    def __init__(self, numero_compte, nom_proprietaire, solde, pourcentage_interets):`

 Sur le CompteCourant, l'utilisateur a la possibilité de faire un retrait, et le programme doit prendre en compte la 
gestion d'agios, qui se mettent en place si : le compte courant de l'utilisateur, à l'issue d'une opération de retrait, se retrouve
 dans le négatif, ou si le découvert maximum autorisé est dépassé.

"""Le programme doit demander à l’utilisateur le compte concerné (« courant » ou « epargne ») et le montant
de la transaction (positif pour un versement, négatif pour un retrait)
Chaque appel de méthode doit afficher le solde avant opération, le détail de l’opération et le solde après
opération. On suppose pour la simplicité de l’exercice que chaque modification du solde applique les agios
ou intérêts du compte modifié.De plus, une classe fille CompteCourant, qui ajoute une gestion du découvert
(montant maximum négatif possible) et des agios (pénalité de X % si le solde est inférieur à zéro) :"""

# TODO:implémenter une interface permettant à l'utilisateur d'entrer ses coordonnées bancaires
# TODO:remplacer les print par des return
# TODO:travailler avec les bases de données
# TODO:fonction retrait et appliquer agios
# TODO:corriger méthode appliquer_agios et retrait

class Compte:
    def __init__(self, numero_compte, nom_proprietaire, solde):
        self.__numero_compte = numero_compte
        self.__nom_proprietaire = nom_proprietaire
        self.__solde = solde

    """On va ici définir nos getter et nos setter, qui vont nous permettre de rappeler les attributs
    de la classe compte dans les méthodes retrait, afficher_solde,et versement."""

    def set_solde(self, nouveau_solde):
        self.__solde = nouveau_solde

    def get_nom_proprietaire(self):
        return self.__nom_proprietaire

    def get_numero_compte(self):
        return self.__numero_compte

    def get_solde(self):
        return self.__solde

    def get_autorisation_decouvert(self):
        return self.autorisation_decouvert

    def get_agios(self):
        return self.pourcentage_agios


    """Cette méthode affiche le compte courant de l'utilisateur sans autre opération."""

    def afficher_solde(self):
        # FIXME
        numero_compte = self.get_numero_compte()
        nom_proprietaire = self.get_nom_proprietaire()
        solde = self.get_solde()
        print("  Bonjour " + str(nom_proprietaire) + "  " + "vous avez " + " " + str(solde) + \
              " " + "euros sur votre compte n°" + " " + str(numero_compte))

    """La méthode retrait permet de retirer de l'argent depuis le compte courant, en affichant l'état du solde
    avant et après l'opération de retrait."""

    def retrait(self, montant_retrait: int):
        solde_actuel = self.get_solde()
        print("Votre solde est de : " + str(solde_actuel) + " euros")
        self.set_solde(solde_actuel - montant_retrait)
        print("Nouveau solde : {} euros".format(self.__solde, montant_retrait))

    """La méthode versement permet de verser de l'argent sur le compte courant de l'utilisateur, en affichant
    l'état du solde avant et après l'opération de versement."""

    def versement(self, montant_versement):
        solde_actuel = self.get_solde()
        print("Votre solde est de :  " + str(solde_actuel) + " euros")
        self.set_solde(solde_actuel + montant_versement)
        print("Nouveau solde : {} euros".format(self.__solde, montant_versement))


"""La classe abstraite CompteCourant permet d'implémenter les méthodes """


class CompteCourant(Compte):
    def __init__(self, numero_compte, nom_proprietaire, solde, \
                 autorisation_decouvert, pourcentage_agios):
        self.autorisation_decouvert = autorisation_decouvert
        self.pourcentage_agios = pourcentage_agios
        """On surcharge la méthode pour pouvoir utiliser les attributs de la classe Compte\
        dans les méthodes appliquer_agios"""
        super().__init__(numero_compte, nom_proprietaire, solde)

    def retrait(self, montant_retrait: int):
        solde_actuel = self.get_solde()
        max_decouvert = self.get_autorisation_decouvert()
        if solde_actuel - montant_retrait >= max_decouvert:
            super().retrait(montant_retrait)
            self.appliquer_agios(montant_retrait)
        pass



    """Méthode permettant le calcul et l'application des agios sur le compte épargne."""

    def appliquer_agios(self, montant_retrait):
        # FIXME:
        max_decouvert = self.get_autorisation_decouvert()
        agios = self.get_agios()
        solde_actuel = self.get_solde()
        if montant_retrait > max_decouvert:
            super().retrait(montant_retrait)
            self.set_agios((agios / 100 )* solde_actuel + solde_actuel)
            print("Découvert de {}:".format(self.appliquer_agios))
        else:
            print("Solde actuel :".format(self.appliquer_agios))

    def set_decouvert(self, max_decouvert):
        self.autorisation_decouvert = max_decouvert

    def set_agios(self):
        return self.pourcentage_agios


class CompteEpargne(Compte):
    def __init__(self, numero_compte, nom_proprietaire, solde, pourcentage_interets):
        """Surcharge de la classe CompteEpargne afin de récupérer les attributs de la classe\
                compte et les réutiliser dans la méthode appliquer_interets"""
        super().__init__(numero_compte, nom_proprietaire, solde)
        self.__pourcentage_interets = pourcentage_interets

    """On définit des getter et setter pour réutiliser les attributs de la classe Compte"""
    def get_pourcentage_interets(self):
        return self.__pourcentage_interets

    """Méthode pour calculer les intérêts"""
    def appliquer_interets(self, versement):
        taux_interets = self.get_pourcentage_interets()
        solde_actuel = self.get_solde()
        if solde_actuel > 0:
            self.set_solde((taux_interets/100 * versement)+solde_actuel)
            super().versement(versement)
            print("Vous avez :" + str(solde_actuel) + " euros sur votre compte épargne")
        else:
            print("solde actuel :" + str(self.get_solde()) +" euros")




# TODO:Tests du code
#x1 = CompteCourant(123, "bla", 150, -100, 12)
#x1.afficher_solde()
#x1.retrait(160)
#x1.appliquer_agios()

#x2 = CompteEpargne(31234,"egg",1000,75)
x1 = CompteCourant(123, "bla", 150, -100, 12)
#x2.afficher_solde()
#x2.appliquer_interets(100)
#x2.retrait(100)
x1.afficher_solde()
x1.retrait(160)
x1.appliquer_agios(150)